var exports = module.exports = {};
var OrientDB = require('orientjs');

var OrientDB = require('orientjs');
var server = OrientDB({
    host: '141.69.56.164',
    port: 2424,
});

var db = server.use({
    name: 'RouteDB',
    username: 'root',
    password: 'secret'
});

/**
 * Get active members.
 * @param lat center point lat
 * @param lng center point lng
 * @param radius center radius
 * @param maxTimeDistance maximum time to be considered 'active'
 */
exports.getActiveMembers = async function(lat,lng,radius,maxTimeDistance){
    let routes = await db.select().from('Route').all();
    let returnRoutes = [];
    routes.map((route) => {
        if(getDistanceBetweenInKm(lat,lng,route.lat,route.lng) <= radius){
            //distance in minutes
            if( minutesBetweenDates(new Date(),new Date(route.time)) < maxTimeDistance){
                returnRoutes.push(route);
            }
        }
    });
    return returnRoutes;
};


minutesBetweenDates = (date1,date2) => {
    return Math.abs(date1 - date2) / 60000;
};

getAllPointsInRadius = async (lat,lng,radius,vehicle) => {
    let routes = await db.select().from('Route').all();
    let returnRoutes = [];
    routes.map((route) => {
        if(getDistanceBetweenInKm(lat,lng,route.lat,route.lng) <= radius && vehicle == route.vehicle_type){
            returnRoutes.push(route);
        }
    });
    return returnRoutes;
};



getRouteByIdSortedByTimeDesc = async (routeId) => {
    await db.open();
    let res = await db.query('select * from Route where id = '+routeId+' order by time desc;');
    return res;
};


getDistanceBetweenInKm = (lat1,lng1,lat2,lng2) => {
    let lat1_rad = lat1 * Math.PI / 180;
    let lng1_rad = lng1 * Math.PI / 180;
    let lat2_rad = lat2 * Math.PI / 180;
    let lng2_rad = lng2 * Math.PI / 180;
    return  ( 6371 * Math.acos( Math.cos( lat1_rad ) * Math.cos( lat2_rad ) * Math.cos( lng2_rad - lng1_rad ) + Math.sin( lat1_rad ) * Math.sin( lat2_rad ) ) )
};


MAX_DISTANCE_TO_BE_EQUAL = 0.1;
arePointsEqual = (point1,point2) => {
    if(getDistanceBetweenInKm(point1.lat,point1.lng,point2.lat,point2.lng) > MAX_DISTANCE_TO_BE_EQUAL){
        return false;
    }
    return true;
};


cutPointsToRelevantPart = (points,startpoint,endpoint) => {
    pointsReturn = [];
    relevantPoints = false;
        points.map((point) => {
           if(getDistanceBetweenInKm(point.lat,point.lng,startpoint.lat,startpoint.lng) <= POINT_SURROUNDING){
                relevantPoints=true;
           }
            if(getDistanceBetweenInKm(point.lat,point.lng,endpoint.lat,endpoint.lng) <= POINT_SURROUNDING){
                relevantPoints=false;
            }
           if(relevantPoints){
               pointsReturn.push(point);
           }
        });
        return pointsReturn;
};

POINT_SURROUNDING = 2000000;

areRoutesEqual = (points1,points2,startpoint,endpoint) => {
    let biggerPoints = points1.length>points2.length ? points1 : points2;
    let smallerPoints = points1.length<points2.length ? points1 : points2;
    //find first equal point
    let startSmaller = -1;
    let startBigger = -1;
    for(let i=0;i<biggerPoints.length;i++){
        for(let j=0;j<smallerPoints.length;j++){
            if(getDistanceBetweenInKm(biggerPoints[i].lat,biggerPoints[i].lng,smallerPoints[j].lat,smallerPoints[j].lng) <= MAX_DISTANCE_TO_BE_EQUAL){
                startSmaller = j;
                startBigger = i;
            }
        }
    }
    if(startSmaller == -1){
        return false;
    }
    let routeOver=false;
    while(!routeOver){
        if(getDistanceBetweenInKm(biggerPoints[startBigger].lat,biggerPoints[startBigger].lng,smallerPoints[startSmaller].lat,smallerPoints[startSmaller].lng) >= MAX_DISTANCE_TO_BE_EQUAL){
            return false;
        }
        if(getDistanceBetweenInKm(biggerPoints[startBigger].lat,biggerPoints[startBigger].lng,endpoint.lat,endpoint.lng) <= MAX_DISTANCE_TO_BE_EQUAL){
            return true;
        }
        startBigger+=1;
        startSmaller+=1;
        if(startBigger>=biggerPoints.length || startSmaller>=smallerPoints.length){
            return false;
        }
    }
};

exports.getRoutes = async function(startpoint,endpoint,vehicle){
    let startRouteIds = [];
    let endRouteIds = [];
    let startSurrounding = await getAllPointsInRadius(startpoint.lat,startpoint.lng,POINT_SURROUNDING,vehicle);
    startSurrounding.map((point) =>{
       if(startRouteIds[point.id]==null){
           startRouteIds.push({id:point.id,time:point.time,vehicle_type:point.vehicle_type});
       }
    });
    let endSurrounding = await getAllPointsInRadius(endpoint.lat,endpoint.lng,POINT_SURROUNDING,vehicle);
    endSurrounding.map((point) =>{
        if(endRouteIds[point.id]==null){
            endRouteIds.push({id:point.id,time:point.time,vehicle_type:point.vehicle_type});
        }
    });
    //routes that go from startpoint to endpoint
    let mutualRouteIds = [];
    endRouteIds.map((endPoint) =>{
       startRouteIds.map((startPoint) => {
           if(startPoint.id == endPoint.id){
               if(endPoint.time > startPoint.time){ //only into right direction
                   mutualRouteIds[startPoint.id] = startPoint.id;
              }
           }
       });
    });
    /*
    {
        id,
        startTime,
        endTime,
        points,

    }
     */
    //first all routes into object
    let selectedRoutes = [];
    await Promise.all(mutualRouteIds.map(async (routeId) => {
        let routePoints = await getRouteByIdSortedByTimeDesc(routeId);
        let route = {
            id: routePoints[0].id,
            startTime: null,
            endTime: null,
            points: []
        };
        routePoints.map((routePoint) =>{
            route.points.push({
                time: routePoint.time,
                lat: routePoint.lat,
                lng: routePoint.lng
            });

        });
        route.endTime = route.points[0].time;
        route.startTime = route.points[route.points.length-1].time;
        selectedRoutes.push(route);
    }));
    selectedRoutes.map((route) => {
        //route.points = cutPointsToRelevantPart(route.points,startpoint,endpoint);
    });
    //equal routes : bsp: {89,90},{99,192} 89 and 90 are equal
    let equalRoutes = [];
    let usedIndices = [];
    var i;
    for(i=0;i<selectedRoutes.length;i++){
        let alreadyUsedIndex=false;
        for(let k=0;k<usedIndices.length;k++){
            if(usedIndices[k] == i){
                alreadyUsedIndex=true;
            }
        }
        if(alreadyUsedIndex){
            continue;
        }
        usedIndices.push(i);
        currentEquals = [];
        currentEquals.push(selectedRoutes[i]);
        let j;
        for(j=i+1;j<selectedRoutes.length;j++){
            //sind sie gleich
            if(areRoutesEqual(selectedRoutes[i].points,selectedRoutes[j].points,startpoint,endpoint)){
                usedIndices.push(j);
                currentEquals.push(selectedRoutes[j]);
            }
        }
        equalRoutes.push(currentEquals);
    }
    console.log(equalRoutes);
    let returnRoutes = [];
    // {averageTime,points}
    for(let i=0;i<equalRoutes.length;i++){
        let averageTime = 0;
        for(let j=0;j<equalRoutes[i].length;j++){
            averageTime += equalRoutes[i][j].endTime - equalRoutes[i][j].startTime;
        }
        averageTime = (averageTime / equalRoutes[i].length) / 60000; //average in minutes
        returnRoutes.push({averageTime:averageTime,points:equalRoutes[i][0].points});
    }
    return returnRoutes;
};