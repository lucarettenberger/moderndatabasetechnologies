const io = require('socket.io')();
const db = require('./database');


RADIUS = 100;
MAX_TIME_DISTANCE = 1000000000000;
REFRESH_RATE = 1000;

let subscribedToTrafficUpdates = {};
let subscribedToRouteSearch = {};
io.on('connection', (client) => {
    console.log('new user connected');
    subscribedToTrafficUpdates[client.id] = null;
    client.on('subscribeToTrafficUpdates', (lat,lng) => {
        subscribedToTrafficUpdates[client.id] = setInterval(() => {
                db.getActiveMembers(lat,lng,RADIUS,MAX_TIME_DISTANCE ).then((members) => {
                    client.emit('timer',members);
                })
            }, REFRESH_RATE);
    });

    client.on('subscribeToRouteSearch', (point1,point2,vehicle) => {
        subscribedToRouteSearch[client.id] = setInterval(() => {
            db.getRoutes(point1,point2,parseInt(vehicle)).then((routes) => {
                client.emit('routes',routes);
            })
        }, REFRESH_RATE);
    });

    client.on('unsubscribeToTrafficUpdates', () => {
        clearInterval(subscribedToTrafficUpdates[client.id]);
    });

    client.on('unsubscribeToRouteSearch', () => {
        clearInterval(subscribedToRouteSearch[client.id]);
    });

    client.on("disconnect", () => {
        clearInterval(subscribedToTrafficUpdates[client.id]);
        clearInterval(subscribedToRouteSearch[client.id]);
    });
});

const port = 8000;
io.listen(port);
console.log('listening on port ', port);