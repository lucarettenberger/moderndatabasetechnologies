import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import 'semantic-ui-css/semantic.min.css';
import MapState from "./stores/MapState";
import {Provider} from "mobx-react";

const stores = {
    mapState: MapState
};

ReactDOM.render(
    <Provider {...stores}>
    <App/>
    </Provider>
    ,document.getElementById('root'));