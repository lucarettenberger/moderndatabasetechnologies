import React, { Component } from "react";
import Geosuggest from 'react-geosuggest';
import {Label,Select,Divider,Form} from 'semantic-ui-react'
import {setFoundRoutesUpdatesCallback, subscribeToRouteSearch} from './../stores/sockets';
import {inject, observer} from "mobx-react";


@inject("mapState")
@observer
class RoutePlaner extends Component {

    constructor(props) {
        super(props);
        setFoundRoutesUpdatesCallback(((err, members) => this.updateFoundRoutes(err,members)));
    }


    vehicle = -1;
    handleVehicleChange = (e,data) =>(this.vehicle = data.value);;

    startCity = '';
    handleStartChange = (e) => (this.startCity = e);

    endCity = '';
    handleEndChange = (e) => (this.endCity = e);

    clickRoutePlaning = () => {
        if(this.startCity == '' || this.endCity == '' || this.vehicle == -1){
            return;
        }
        subscribeToRouteSearch(this.startCity.location,this.endCity.location,this.vehicle);
    };

    updateFoundRoutes = (err,routes) => {
        this.props.mapState.setRoutes(routes);
    };

    render() {
        var countryOptions = [{ key: 'bike', value: '1', text: 'Bike' },{ key: 'car', value: '0', text: 'Car' }];
        return (
            <div>
                <h3> Route Planer </h3>
                <Form onSubmit={this.clickRoutePlaning}>
                    <Form.Group widths='equal'>
                        <Form.Field>
                            <Select placeholder='Select your vehicle' options={countryOptions} onChange={this.handleVehicleChange} />
                        </Form.Field>
                        <Form.Field>
                            <Geosuggest autoComplete={'off'} placeholder={'Enter Start Location'} onSuggestSelect={this.handleStartChange} />
                        </Form.Field>
                        <Form.Field>
                            <Geosuggest autoComplete={'off'} placeholder={'Enter End Location'} onSuggestSelect={this.handleEndChange} />
                        </Form.Field>
                        <Form.Button fluid>Find Routes!</Form.Button>
                    </Form.Group>
                </Form>
            </div>
        );
    }
}

export default RoutePlaner;
