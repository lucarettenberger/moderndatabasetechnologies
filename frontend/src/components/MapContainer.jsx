import React from "react";
import {Map, Marker, GoogleApiWrapper,Polyline} from 'google-maps-react';
import car from '../images/car.svg'
import bicycle from '../images/bicycle.svg'
import {inject, observer} from "mobx-react";

@inject("mapState")
@observer
export default class MapContainer extends React.Component {


    setMarkers = (markers) => {
        let markersReturn = [];
        markers.map((marker) => {
            markersReturn.push(<Marker position={{ lat: marker.lat, lng: marker.lng }} lastActive={marker.time} icon={this.getMarker(marker.vehicle_type)}/>)
        });
        return markersReturn;
    };

    setRoutes = (routes) => {
        let routesReturn = [];
        console.log('ni');
        routes.map((route) => {
            let points = [];
            for(let i=0;i<route.points.length;i++){
                points.push({lat:route.points[i].lat,lng:route.points[i].lng});
            }
            routesReturn.push(<Polyline path={points} strokeColor="#0000FF" strokeOpacity={0.8} strokeWeight={2} />)
        });
        console.log(routesReturn);
        return routesReturn;
    };

    getMarker = (verhicle_type) =>{
        switch (verhicle_type) {
            case 0: return car;
            case 1: return bicycle;
        }
    };

    render() {
        let {mapCenter,activeUsersNewestMarker,routes} = this.props.mapState;
        return (
            <Map google={this.props.google}
                 initialCenter={{lat: mapCenter.lat, lng: mapCenter.lng}}
                 center={{lat: mapCenter.lat, lng: mapCenter.lng}}
                 zoom={14}
                 draggable={false}
                 minZoom={11}>
                {this.setMarkers(activeUsersNewestMarker)}
                {this.setRoutes(routes)}
            </Map>
        );
    }
}
