import React, { Component } from "react";
import MapContainer from "./MapContainer";
import {GoogleApiWrapper} from "google-maps-react";
import {Input,Select,Divider,Form,Container} from 'semantic-ui-react';
import {inject, observer} from "mobx-react";
import RoutePlaner from "./RoutePlaner";
import CityFinder from "./CityFinder";

@inject("mapState")
@observer

export class App extends Component {

    render() {
        return (
            <div>
                <h1>Routes</h1>
                <RoutePlaner/>
                <CityFinder/>
                <MapContainer google={this.props.google}/>
            </div>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: ('AIzaSyCVAi3beCa7FiRSKIvy6Z7ZDOnGeM7heY8')
})(App)