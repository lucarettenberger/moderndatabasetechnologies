import React, { Component } from "react";
import {Input,Select,Divider,Form,Container} from 'semantic-ui-react'
import "../styles/geo.css"
import Geosuggest from "react-geosuggest";
import {inject, observer} from "mobx-react";
import { subscribeToTrafficUpdates,setTrafficUpdatesCallback } from './../stores/sockets';

@inject("mapState")
@observer
class CityFinder extends Component {

    constructor(props) {
        super(props);
        setTrafficUpdatesCallback(((err, members) => this.updateActiveMembers(err,members)));
    }


    city = '';
    handleChange = (e) => (this.city = e);

    handleSearchCityClick = () => {
        if(this.city == '' || this.city == null){
            return;
        }
        this.props.mapState.setMapCenter({
            lat: this.city.location.lat,
            lng: this.city.location.lng
        });
        subscribeToTrafficUpdates(this.city.location.lat,this.city.location.lng);
    };

    updateActiveMembers = (err,members) => {
        let membersState = [];
        members.map((member) =>{
           membersState.push({
               id: member.id,
               time: member.time,
               lat: member.lat,
               lng: member.lng,
               vehicle_type: member.vehicle_type
           });
        });
        this.props.mapState.setMarkers(membersState);
    };

    render() {
        return (
            <div>
                <h3> City Finder </h3>
                <Form onSubmit={this.handleSearchCityClick}>
                    <Form.Group widths='equal'>
                        <Form.Field>
                            <Geosuggest autoComplete={'off'} placeholder={'Enter City'} label='City' type='city' name='city' onSuggestSelect={this.handleChange}/>
                        </Form.Field>
                        <Form.Button fluid>Show Your City!</Form.Button>
                    </Form.Group>
                </Form>
            </div>
        );
    }
}

export default CityFinder;
