import openSocket from 'socket.io-client';
const  socket = openSocket('http://localhost:8000');

function setTrafficUpdatesCallback(cb){
    socket.on('timer', timestamp => cb(null, timestamp));
}

function setFoundRoutesUpdatesCallback(cb){
    socket.on('routes', routes => cb(null, routes));
}

function subscribeToTrafficUpdates(lat,lng) {
    socket.emit('unsubscribeToTrafficUpdates');
    socket.emit('subscribeToTrafficUpdates', lat,lng);
}

function subscribeToRouteSearch(point1,point2,vehicle) {
    socket.emit('unsubscribeToRouteSearch');
    socket.emit('subscribeToRouteSearch', point1,point2,vehicle);
}


export { subscribeToTrafficUpdates,setTrafficUpdatesCallback,subscribeToRouteSearch,setFoundRoutesUpdatesCallback};