import {observable, action, computed} from "mobx";
import axios from "axios";

class MapState {
  @observable mapCenter;
  @observable markers;
  @observable routes;

  constructor() {
    this.mapCenter = {
      lat: 47.81009,
      lng: 9.63863
    };
    this.markers = [];
    this.routes = [];

  }
    @action setMapCenter(data) {
        this.mapCenter = data;
    }

    @action setMarkers(data){
        this.markers = data;
    }

    @action setRoutes(data){
        this.routes = data;
    }

    @computed get activeUsersNewestMarker(){
        let latestMarkers = [];
        this.markers.map((marker) =>{
          let found = false;
          for(let i=0;i<latestMarkers.length;i++){
              if(latestMarkers[i].id == marker.id){
                  found=true;
                  if(marker.time > latestMarkers[i].time){
                      latestMarkers[i] = marker;
                  }
              }
          }
          if(!found){
              latestMarkers.push(marker);
            }
        });
        return latestMarkers;
    }


}

export default new MapState();
