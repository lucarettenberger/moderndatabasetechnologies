import json
import random
from datetime import datetime, timedelta


class UserData:
    def __init__(self,user_id,lat,lng,time,vehicle_type):
        self.user_id = user_id
        self.lat = lat
        self.lng = lng
        self.time = time
        self.vehicle_type = vehicle_type

# https://gist.github.com/rg3915/db907d7455a4949dbe69
def gen_datetime(min_year=1900, max_year=datetime.now().year):
    # generate a datetime in format yyyy-mm-dd hh:mm:ss.000000
    start = datetime(min_year, 1, 1, 00, 00, 00)
    years = max_year - min_year + 1
    end = start + timedelta(days=365 * years)
    return start + (end - start) * random.random()


user_data = list()
with open('data_json.txt') as json_file:
    data = json.load(json_file)
    user_id = 0
    for p in data['features']:
        latlng = p['geometry']['paths'][0]
        vehicle = round(random.uniform(0, 1))
        time = gen_datetime(min_year=2018)
        for coord in latlng:
            user_data.append(UserData(user_id,coord[1],coord[0],time.isoformat()[:-3],vehicle))
            if vehicle == 0: time = time + timedelta(seconds=10)
            else: time = time + timedelta(seconds=30)
        user_id+=1


random.shuffle(user_data)

out = open('dummy_data.txt', 'w')
for user in user_data:
    out.write("%d,%f,%f,%s,%d\n"%(user.user_id,user.lat,user.lng,user.time,user.vehicle_type))
out.close()

